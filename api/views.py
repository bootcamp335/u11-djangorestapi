from django.shortcuts import render
from .models import Pelicula
from django.http import HttpResponse, JsonResponse
import json

from django.views.decorators.csrf import csrf_exempt
from .serializer import PeliculaSerializer

from rest_framework.parsers import JSONParser
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status, mixins, generics
from rest_framework.views import APIView


class PeliculasApiView(APIView):
    #Listar Peliculas
    def get(self, request):
        peliculas = Pelicula.objects.all()
        ordenar_por = request.GET.get('ordenarPor', '')
        if ordenar_por:
            peliculas = peliculas.order_by(ordenar_por) 
        respuesta= PeliculaSerializer(peliculas, many=True)
        return Response(respuesta.data)
    
    #crear nueva Pelicula
    def post(self, request):
        serializer = PeliculaSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST) 


class PeliculaApiView(mixins.RetrieveModelMixin,
                      mixins.UpdateModelMixin,
                      mixins.DestroyModelMixin, generics.GenericAPIView):
    queryset = Pelicula.objects.all()
    serializer_class = PeliculaSerializer
    lookup_field = 'id'
    def get(self, request, *args, **kwargs):
       return self.retrieve(request, *args, *kwargs)
       
    def update(self, request, *args, **kwargs):
       return self.update(request, *args, *kwargs)
   
    def delete(self, request, *args, **kwargs):
       return self.delete(request, *args, *kwargs)
   
   
   

# Create your views here.
@csrf_exempt
@api_view(['GET', 'POST'])
def peliculas(request):
    if request.method == "GET":
        peliculas = Pelicula.objects.all()
        ordenar_por = request.GET.get('ordenarPor', '')
        if ordenar_por:
            peliculas = peliculas.order_by(ordenar_por) 
        respuesta= PeliculaSerializer(peliculas, many=True)
        return Response(respuesta.data)
        
        #return JsonResponse(respuesta.data, safe=False)
        
        # for pelicula in peliculas:
        #         dict={}
        #         dict['titulo'] = pelicula.titulo
        #         dict['sinopsis'] = pelicula.sinopsis
        #         dict['estreno'] = pelicula.estreno
        #         respuesta.append(dict) 
   
    if request.method == 'POST':        
        serializer = PeliculaSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST) 
        
                # if request.content_type != 'application/json':
        #     return HttpResponse(status=400) #bad request     
        #data = JSONParser().parse(request)
            # try:
            #     json_data = json.loads(request.body.decode())
            # except json.JSONDecodeError:
            #     return HttpResponse(status=400) 
            
             # return JsonResponse(serializer.data, status=201)      # using Jsonresponse  
             
        # pelicula = Pelicula(
        #     titulo = json_data.get('titulo', ''),
        #     estreno = json_data.get('estreno', ''),
        #     sinopsis = json_data.get('sinopsis', '')
        # )
        #pelicula.save()

        # respuesta = {
        #     'id' : pelicula.id,
        #     'titulo' : pelicula.titulo,
        #     'sinopsis' : pelicula.sinopsis
        # }
        # return JsonResponse(respuesta, status=201)  #created
            
            

            
@csrf_exempt
@api_view(['GET', 'PUT', 'DELETE'])
def pelicula(request, id):
    try:
        pelicula = Pelicula.objects.get(pk=id) 
    except:
        return Response(status=status.HTTP_404_NOT_FOUND)
        #return HttpResponse(status=400)
    
    if request.method == 'GET':
        respuesta = PeliculaSerializer(pelicula)
        return Response(respuesta.data)
        #return JsonResponse(respuesta.data, safe=False)
                # respuesta = {
                #     "titulo": pelicula.titulo,
                #     "sinopsis": pelicula.sinopsis
                # }
    elif request.method == 'PUT':
        #data = JSONParser().parse(request)
        serializer = PeliculaSerializer(pelicula, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)        
        return Response(serializer.errors, status=status.HTTP_404_NOT_FOUND)
            #return JsonResponse(serializer.data) 
        #return JsonResponse(serializer.errors, status=400)
    
    elif request.method == 'DELETE':
        pelicula.delete()
        return HttpResponse(status=200)