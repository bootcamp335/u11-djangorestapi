from django.urls import path, include
from .views import peliculas, pelicula, PeliculasApiView, PeliculaApiView

urlpatterns = [
    #path('peliculas/', peliculas),
    path('peliculas/', PeliculasApiView.as_view()),
    path('peliculas/<int:id>', PeliculaApiView.as_view())
    
]
